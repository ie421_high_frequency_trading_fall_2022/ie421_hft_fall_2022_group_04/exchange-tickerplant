#include <vector>
#include <iostream>
#include <thread>
#include <signal.h>
#include "tickerplant/tickerplant.h"

using std::vector;
using std::thread;

void sigint_handler(int sig) {
  std::cout << "SIGINT received" << std::endl;
  exit(0);
}

int main() {
  {
    // Register signal and signal handler
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = sigint_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
  }

  vector<thread> threads;
}