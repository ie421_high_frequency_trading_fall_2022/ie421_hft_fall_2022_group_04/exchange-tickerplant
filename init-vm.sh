sudo apt-get update
sudo apt-get upgrade -y

# Install C++
sudo apt-get install build-essential -y
gcc --version

# Install Valgrind
sudo apt-get -y install valgrind

# Install GDB
sudo apt-get -y install gdb

# Install CMake3
sudo apt-get install cmake -y
cmake --version

# Install Doxygen
sudo apt-get -y install doxygen
doxygen --version

# Install Catch2
sudo apt-get -y install catch2
catch2 --version

# Install Ninja
sudo apt-get install ninja-build
ninja --version

# Git
# Set default editor to vim
git config --global core.editor "vim"
git config credential.helper store